package com.boomsecret.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wangjinliang on 2019/3/12.
 */
@RestController
public class TestController {

    @RequestMapping("/test")
    public String test() {
        return "provider-9201";
    }
}
