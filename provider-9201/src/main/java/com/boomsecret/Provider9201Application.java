package com.boomsecret;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @author wangjinliang on 2019/3/12.
 */
@SpringBootApplication
@EnableEurekaClient
public class Provider9201Application {
    public static void main(String[] args) {
        SpringApplication.run(Provider9201Application.class, args);
    }
}
