package com.boomsecret.controller;

import com.boomsecret.config.CommonConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wangjinliang on 2019/3/13.
 */
@RestController
public class TestController {
    @Autowired
    private CommonConfig commonConfig;

    @RequestMapping(value = "/test")
    public String test(){
        return commonConfig.getKey();
    }
}
