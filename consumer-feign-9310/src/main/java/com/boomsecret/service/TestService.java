package com.boomsecret.service;

import com.boomsecret.service.TestService.TestServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author wangjinliang on 2019/3/13.
 */
@FeignClient(value = "PROVIDER", fallback = TestServiceImpl.class)
public interface TestService {
    @RequestMapping(value = "/test", method = RequestMethod.GET)
    String test();

    @Component
    class TestServiceImpl implements TestService {

        @Override
        public String test() {
            return "feign error";
        }
    }
}
