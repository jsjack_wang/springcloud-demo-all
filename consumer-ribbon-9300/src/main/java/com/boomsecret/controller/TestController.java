package com.boomsecret.controller;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * @author wangjinliang on 2019/3/12.
 */
@RestController
public class TestController {

    @Autowired
    private RestTemplate restTemplate;

    @RequestMapping("/test")
    @HystrixCommand(fallbackMethod = "error")
    public String test() {
        return "ribbon " + restTemplate.getForObject("http://PROVIDER/test", String.class);
    }

    public String error() {
        return "ribbon error";
    }
}
