# springcloud-demo-all

#### 服务调用方式
    
    ribbon + restTemplate
    feign

#### 框架版本

    springboot      2.0.3.RELEASE
    springcloud     Finchley.RELEASE
    
#### 项目结构
 
    eureka-server-9100          9100
    provider-9200               9200
    provider-9201               9201
    consumer-ribbon-9300        9300 
    consumer-feign-9310         9310
    service-zuul-9400           9400
    config-server-9500          9500
    config-client-9501          9501
    
#### eureka-server-9100 

    Eureka作为服务注册与发现的组件
    
    http://eureka.com:9100/  (eureka.com是hosts配置的)
        
#### provider-9200  

    服务提供者1
    
#### provider-9201
    
    服务提供者2，与provider-9200没有区别，只是看起来项目清晰
    
#### consumer-ribbon-9300 
    
    ribbon + restTemplate消费服务，ribbon用于负载均衡
    
#### consumer-feign-9310

    Feign 采用的是基于接口的注解
    Feign 整合了ribbon，具有负载均衡的能力
    整合了Hystrix，具有熔断的能力
  
#### 熔断器
  
    在微服务架构中，根据业务来拆分成一个个的服务，服务与服务之间可以相互调用（RPC），在Spring Cloud可以用RestTemplate+Ribbon和Feign来调用。
    为了保证其高可用，单个服务通常会集群部署。由于网络原因或者自身的原因，服务并不能保证100%可用，如果单个服务出现问题，调用这个服务就会出现线程阻塞，
    此时若有大量的请求涌入，Servlet容器的线程资源会被消耗完毕，导致服务瘫痪。服务与服务之间的依赖性，故障会传播，会对整个微服务系统造成灾难性的严重后果，
    这就是服务故障的“雪崩”效应。为了解决这个问题，业界提出了断路器模型。

#### service-zuul-9400(路由网关)

    Zuul的主要功能是路由转发和过滤器。路由功能是微服务的一部分，比如/api/user转发到到user服务，/api/shop转发到到shop服务
    zuul默认和Ribbon结合实现了负载均衡的功能
    
    zuul功能
        Authentication
        Insights
        Stress Testing
        Canary Testing
        Dynamic Routing
        Service Migration
        Load Shedding
        Security
        Static Response handling
        Active/Active traffic management
        
#### config-server-9500 (配置中心组件)
    
    http://localhost:9500/test/dev (test-dev.properties)
    
#### config-client-9501

    使用配置中心property的客户端
    
#### 总结
  
###### 关于服务提供者

    一份服务提供者代码，部署在多台机器上，他们可以用disconf共享配置文件    
    
源码地址：https://gitee.com/jsjack_wang/springcloud-demo-all    